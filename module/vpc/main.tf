resource "google_compute_network" "net" {
  
  name                    = var.net_name
  auto_create_subnetworks = false
  
}


resource "google_compute_subnetwork" "subnet" {

  name          =  var.subnet_name
  ip_cidr_range =  var.cidr_ip_range #"10.2.0.0/16"
  region        =  var.region
  network       = "${google_compute_network.net.self_link}"

}

resource "google_compute_firewall" "default" {
  name    = var.firewall_name
  network = google_compute_network.net.name

  allow {
    protocol = "icmp"
  }

  allow {
    protocol = "tcp"
    ports    = ["80", "8080", "22"]
  }

  
}