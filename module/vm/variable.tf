variable "machine_type"{
    type = string
    description = "A Machine to be used for gcp"
 
}
variable "machine_name"{
    type = string
    description = "A Machine to be used for gcp"
 
}
variable "image"{
    type = string
 
    description = "A Machine to be used for gcp"
}
variable "size"{
    type = number
 
    description = "A Machine to be used for gcp"
}
variable "subnet" {
   type= string
   description = "subnet to be used by machines"
}
variable "region" {
   type = string
}
variable "zone" {
   type= string  
}


