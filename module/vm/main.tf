
resource "google_compute_instance" "vm_instance" {
  name         = var.machine_name
  machine_type = var.machine_type
  # region  =  var.region
  zone =  var.zone
  boot_disk {
    initialize_params {
      image = var.image
      size = var.size
    }
  }

    network_interface {
    subnetwork = var.subnet

    access_config {
      
    }
  }
  }




