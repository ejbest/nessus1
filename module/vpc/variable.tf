variable "subnet_name" {
    type = string
    description = "Name of subnetwork"
  
}
variable "cidr_ip_range" {
    type = string
    description = "range of ip in subnet"
  }
variable "region" {
    type = string
    description = "Reigion of subnet"
  }
 variable "net_name" {
    type = string
    description = "Name of subnetwork"
  
}   
variable "firewall_name" {
    type = string
    description = "Name of Firewall to be displayed"
  
} 